/*Hacer un programa que pase de Kg a otra unidad de medida de masa,
mostrar en pantalla un menu con las opciones posible.*/

import javax.swing.*;

public class Ejercicio1 {

    public static void main(String[] args){
        // Variables con las cuales se trabajara.
        float Kg, Hg, Dg, G, Deg, Cg, Mg, total;
        int n;

        JOptionPane.showMessageDialog(null, "Conversor de unidades de masa");
        // Nos pide ingresar un dato por teclado.
        Kg = Float.parseFloat(JOptionPane.showInputDialog(null, "Ingrese unidades unicamente en Kilogramos"));
        n = Integer.parseInt(JOptionPane.showInputDialog(null, "Indique la opcion\n"
        + "1. Hectogramo\n"
        + "2. Decagramo\n"
        + "3. Gramo\n"
        + "4. Decigramo\n"
        + "5. Centigramo\n"
        + "6. Miligramo\n"
        + "7. Salir"));
        // Utilizamos un condicional de selección.
        switch (n){
            case 1: total = Kg * 10;
                    JOptionPane.showMessageDialog(null, "Hectogramo: "+total);
                    break;
            case 2: total = Kg * 100;
                JOptionPane.showMessageDialog(null, "Decagramo: "+total);
                break;
            case 3: total = Kg * 1000;
                JOptionPane.showMessageDialog(null, "Gramo: "+total);
                break;
            case 4: total = Kg * 10000;
                JOptionPane.showMessageDialog(null, "Decigramo: "+total);
                break;
            case 5: total = Kg * 100000;
                JOptionPane.showMessageDialog(null, "Centigramo: "+total);
                break;
            case 6: total = Kg * 1000000;
                JOptionPane.showMessageDialog(null, "Miligramo: "+total);
                break;
            default: break;
        }
    }
}
