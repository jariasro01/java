import java.util.Scanner;

public class Bucles {

    public static void main(String[] args){
        Scanner entrada = new Scanner(System.in);
        int n, i=0;

        System.out.println("Ingrese el numero hasta donde quiere que se ejecute el programa: ");
        n = entrada.nextInt();

        while (i < n){
            System.out.println(i);
            i++;
        }
    }
}
